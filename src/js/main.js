$(document).ready(function() {
	$('.submit').click(function() {
		var formFullName = $('.formFullName').val();
		var formEmailAddress = $('.formEmailAddress').val();
		var formMobileNumber = $('.formMobileNumber').val();
		var formFatherName = $('.formFatherName').val();
		var formMotherName = $('.formMotherName').val();
		var formPresentAddress = $('.formPresentAddress').val();
		var formPermanentAddress = $('.formPermanentAddress').val();
		var formDateOfBirth = $('.formDateOfBirth').val();
		var formNationality = $('.formNationality').val();
		var formMaritalStatus = $('.formMaritalStatus').val();
		var formGender = $('.formGender').val();
		var formReligion = $('.formReligion').val();
		var formLanguageProficiency = $('.formLanguageProficiency').val();
		var formNOD1 = $('.formNOD1').val();
		var formBU1 = $('.formBU1').val();
		var formPY1 = $('.formPY1').val();
		var formSUB1 = $('.formSUB1').val();
		var formCGPA1 = $('.formCGPA1').val();
		var formNOD2 = $('.formNOD2').val();
		var formBU2 = $('.formBU2').val();
		var formPY2 = $('.formPY2').val();
		var formSUB2 = $('.formSUB2').val();
		var formCGPA2 = $('.formCGPA2').val();
		var formNOD3 = $('.formNOD3').val();
		var formBU3 = $('.formBU3').val();
		var formPY3 = $('.formPY3').val();
		var formSUB3 = $('.formSUB3').val();
		var formCGPA3 = $('.formCGPA3').val();
		var formReference = $('.formReference').val();

		// Set Value

		$('.name').text(formFullName);
		$('.address').text(formPresentAddress);
		$('.mobile').text(formMobileNumber);
		$('.email').text(formEmailAddress);
		$('.fullName').text(formFullName);
		$('.fatherName').text(formFatherName);
		$('.motherName').text(formMotherName);
		$('.mailingAddress').text(formPresentAddress);
		$('.permanentAddress').text(formPermanentAddress);
		$('.dateOfBirth').text(formDateOfBirth);
		$('.nationality').text(formNationality);
		$('.maritalStatus').text(formMaritalStatus);
		$('.gender').text(formGender);
		$('.relgion').text(formReligion);
		$('.ac1').text(formNOD1);
		$('.uni1').text(formBU1);
		$('.py1').text(formPY1);
		$('.sub1').text(formSUB1);
		$('.cgpa1').text(formCGPA1);
		$('.ac2').text(formNOD2);
		$('.uni2').text(formBU2);
		$('.py2').text(formPY2);
		$('.sub2').text(formSUB2);
		$('.cgpa2').text(formCGPA2);
		$('.ac3').text(formNOD3);
		$('.uni3').text(formBU3);
		$('.py3').text(formPY3);
		$('.sub3').text(formSUB3);
		$('.cgpa3').text(formCGPA3);
		$('#lang').text(formLanguageProficiency);
		$('#reff').text(formReference);

		// Button Function Set

		function AddClassinf() {
			$('.mainSection').addClass('hide');
			$('.cvSection').removeClass('hide');
		}

		if($('input').val() == ''){
			alert('Please Fill Up The Full Form');
   		}
		else {
			AddClassinf();
		}
		});

	//html2png

	$(function() { 
    	$(".getPng").click(function() { 
        	html2canvas($(".section1"), {
            	onrendered: function(canvas) {
                	theCanvas = canvas;
                	document.body.appendChild(canvas);

                	// Convert and download as image 
                	Canvas2Image.saveAsPNG(canvas); 
                	$("#img-out").append(canvas);
                	// Clean up 
                	document.body.removeChild(canvas);
            	}
        	});
    	});
	});

	//validation

	var x = document.getElementsByTagName('input');
});